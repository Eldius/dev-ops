#!/bin/bash


echo "Installing MySQL 5.0.."
sudo apt-get install -qqy debconf-utils
cat << EOF | debconf-set-selections
mysql-server-5.0 mysql-server/root_password password 123Senha
mysql-server-5.0 mysql-server/root_password_again password 123Senha
mysql-server-5.0 mysql-server/root_password seen true
mysql-server-5.0 mysql-server/root_password_again seen true
EOF

apt-get install -y mysql-server
